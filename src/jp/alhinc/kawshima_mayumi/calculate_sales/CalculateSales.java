package jp.alhinc.kawshima_mayumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		//システム後半でも使えるように大きい括弧の中で支店コード及び支店名に関するマップを作る
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		BufferedReader br = null;
		HashMap<String, String> branchDataMap = new HashMap<String, String>();
		//売上ファイルのリストを作成
		ArrayList<File> salesList = new ArrayList<File>();
		HashMap<String, Long> salesDataMap = new HashMap<String, Long>();

		//BufferedReaderクラスの変数 br を空の状態で宣言
		boolean inputResult = input(args[0], "branch.lst", branchDataMap, salesDataMap);
		if(!(inputResult)) {
			return;
		}

		File file = new File(args[0]);
		File files[] =file.listFiles();
		String fileName;

		for(int i =0; i < files.length; i++) {
			//ファイル名を文字列で取得
			fileName = files[i].getName();

			//正規表現を使ってフィルタをかける
			if((fileName.matches("^[0-9]{8}.rcd$")) && ((files[i]).isFile())) {
				//デバックで好きなとことで中断F6でつぎに進む
				salesList.add(files[i]);
			}
		}

		//連番チェック
		for(int i = 0; i < (salesList.size())-1; i++) {

			String numberRcd = salesList.get(i).getName();
			String numberPlusRcd = salesList.get(i+1).getName();
			//なんで８？→文字間の空間を数えてる！
			String number = numberRcd.substring(0, 8);
			String numberPlus = numberPlusRcd.substring(0, 8);

			int n = Integer.parseInt(number);
			int np =Integer.parseInt(numberPlus);

			if(np - n != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
			//salesList=売上ファイルの数だけ繰り返し処理

		for(int i = 0; i < salesList.size(); i++) {
			//i番目の売上ファイルを読み込み
			try {
				File salesFile = salesList.get(i);
				FileReader fr = new FileReader(salesFile);
				br = new BufferedReader(fr);
				String line;

				ArrayList<String> codeSalesList = new ArrayList<String>();
				//売上金額に数字以外が記入されてたらエラーメッセージにしたい。

				//売上ファイル内のテキスト行の数だけ繰り返し処理
				while((line = br.readLine()) != null) {
					//文字列として取得したline をint型にしてリストに加えたい

					codeSalesList.add(line);
				}
				//saleDatemapに売上ファイルの一行目をkey,2行目をvalueのなかに入れたい。
				//初めにsalesDatemapに支店コードとvalueに０を入れておく←支店定義ファイル作成のところで実施
				//単純にputすると重複キーが塗り替えられるから、マップから出して足して仕舞うをループ
				//支店コードに該当がなかった時のエラー処理
				if(codeSalesList.size() != 2) {
					System.out.println(salesFile.getName()+ "のフォーマットが不正です");
					return;
				}
				if(!(codeSalesList.get(1).matches("^[0-9]+$"))){
				 	System.out.println("予期せぬエラーが発生しました");
				 	return;
				}
				if(!(salesDataMap.containsKey(codeSalesList.get(0)))){
					System.out.println(salesFile.getName()+"の支店コードが不正です");
					return;
				}
				long sales = ((salesDataMap.get(codeSalesList.get(0))) +   Long.parseLong(codeSalesList.get(1)));
				if(sales > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesDataMap.put((codeSalesList.get(0)), sales );
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		//出力を行いたい！！
		boolean outputResult = output(args[0], "branch.out", branchDataMap, salesDataMap);
		if(!(outputResult)) {
			return;
		}
	}

	private static boolean input(String path, String fileName, HashMap<String, String> branchMap, HashMap<String, Long> salesMap) {
		BufferedReader br = null;
		try {
			/*Fileクラスに基づくFileというインスタンス生成し、ファイルの場所（args[0]）とファイル名を指定
			 * 変数fileはFileインスタンスを参照されたし*/
			File file = new File(path, fileName);
			//エラー処理　支店定義ファイルの存在確認
			if (!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			//FileReaderクラスを用いて変数fileというファイルを読み込む。
			FileReader fr = new FileReader(file);
			//BufferReaderインスタンスを生成し、中身をfrとして変数brに代入
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] branchDate = line.split(",");

				if(!(branchDate.length == 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				if(!(branchDate[0].matches("^\\d{3}$"))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branchMap.put(branchDate[0], branchDate[1]);
				salesMap.put(branchDate[0], (long) 0);
			}

		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

			//例外事象があっても、なくても最終的にやる処理
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean output(String path, String fileName, HashMap<String, String> branchMap, HashMap<String, Long> salesMap) {
		BufferedWriter bw =null;
		try {
			File outFile = new File(path, fileName);
			FileWriter filewriter = new FileWriter (outFile);
			bw = new BufferedWriter(filewriter);
			for (String key: branchMap.keySet()) {
				bw.write((key)+ "," + branchMap.get(key) + "," + salesMap.get(key));
				//ここで改行
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
